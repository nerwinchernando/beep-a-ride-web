# rake generate_seed
seeds = [
    "car_makes_seeds",
    "car_models_seeds",
    "delivery_options_seeds",
    "driving_rules_seeds",
    "fuel_types_seeds",
    "payment_frequencies_seeds",
    "transmission_types_seeds",
    "vehicle_types_seeds",
    "payment_methods_seeds"
]

task :generate_seed do
    puts "Seeding " + Rails.env + " environment"
    remove_file(Rails.root.join('db', 'schema.rb'), force=true)
    #Rake::Task["db:drop"].invoke
    #Rake::Task["db:create"].invoke
    #Rake::Task["db:migrate"].invoke
    seeds.each do |seed|
        puts "Seeding " + seed + ".rb"
        Rake::Task["db:seed:#{seed}"].invoke
    end

    puts "Seeding is complete! Hurray!!!"

end
