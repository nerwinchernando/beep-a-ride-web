# syntax => rake migrate_customer_info[BeepCars.xlsx]

task :migrate_customer_info, [:file] => :environment do |t,xls|

	puts ""
	print "Migrating Customer Info data."

	#log files (setup)
	start_time = Time.now.strftime('%Y%m%d%H%M%S')
	log_dir = FileUtils.mkpath Rails.root.join('log','migration', 'customer_info')
	log_file = File.new(log_dir[0] + '/' + start_time + '.log', 'w')

	#file migration
	file = Dir.pwd + "/lib/tasks/" + xls[:file]
	spreadsheet = Roo::Spreadsheet.open(file)
	header = spreadsheet.row(1)
	(2..spreadsheet.last_row).each do |i|
	  row = Hash[[header, spreadsheet.row(i)].transpose]
	  print "."

	  generated_password = Devise.friendly_token.first(8)
	  car_owner_email_param = "#{row["CO EMAIL"]}"
	  user = User.new(email: car_owner_email_param, password: generated_password)
	  
	  car_owner_fullname_param = "#{row["CO FULLNAME"]}"
	  # Save User
	  unless user.save then
	  	log_file.puts("Failed : Record #{i} name : #{car_owner_fullname_param}")
	  	log_file.puts("  Reason : #{car_owner_email_param} already exists! " )
	  	user = User.find_by(email: car_owner_email_param)
	  else
	  	log_file.puts("Record #{i}: { email: #{car_owner_email_param}, password: #{generated_password} }")

		  # Save Profile
		  date_time_received_param = "#{row["DT"]}"
		  car_owner_contact_no_param = "#{row["CO CONTACT NUMBER"]}"

		  unless Profile.create(full_name: car_owner_fullname_param, phone_number: car_owner_contact_no_param,
		  		  		      date_time_received: date_time_received_param, user_id: user.id) then
	  	  log_file.puts("Failed : Record #{i} Profile : #{car_owner_fullname_param}")
		  end
	  end

	  vehicle_type_param = "#{row["VEHICLE TYPE"]}"
	  vehicle_type_unknown = VehicleType.find_by(description: "Not Specified")
    vehicle_type = VehicleType.find_by(description: vehicle_type_param)
	  unless vehicle_type.nil? then
	  	vehicle_type_id = vehicle_type.id
	  else
	  	vehicle_type_id = vehicle_type_unknown.id
	    log_file.puts("Failed : Record #{i}: #{vehicle_type_param} unknown.")
	  end

    capacity_param = "#{row["CAPACITY"]}"
    if capacity_param == "" then
      capacity_param = 4
    end

    car_year_param = "#{row["YEAR"]}"
    if car_year_param == "" then
      car_year_param = 1900
    end

    log_file.puts("  -- capacity : #{capacity_param}, car year: #{car_year_param} .")

	  car_model_param = "#{row["MODEL"]}"
	  car_model_unknown = CarModel.find_by(model: "Not Specified")
    car_model = CarModel.find_by(model: car_model_param)
    unless car_model.nil? then
	  	car_model_id = car_model.id
	  else
	  	car_model_id = car_model_unknown.id
	    log_file.puts("  -- #{car_model_param} not found.")
	  end

	  transmission_param = "#{row["TRANSMISSION"]}"
	  transmission_type_unknown = TransmissionType.find_by(description: "Not Specified")
    transmission_type = TransmissionType.find_by(description: transmission_param)
    unless transmission_type.nil? then
	  	transmission_type_id = transmission_type.id
	  else
	  	transmission_type_id = transmission_type_unknown.id
	    log_file.puts("  -- #{transmission_param} not found.")
	  end
	  
	  fuel_param = "#{row["FUEL"]}"
	  fuel_type_unknown = FuelType.find_by(description: "Not Specified")
    fuel_type = FuelType.find_by(description: fuel_param)
    unless fuel_type.nil? then
	  	fuel_type_id = fuel_type.id
	  else
	  	fuel_type_id = fuel_type_unknown.id
	    log_file.puts("  -- #{fuel_param} not found.")
	  end

    color_param = "#{row["COLOR"]}"
    if color_param == "" then
      color_param = "Not Specified"
    end
	  
	  mileage_param = "#{row["MILEAGE"]}"
    if mileage_param == "" then
      mileage_param = 0
    end

	  log_file.puts("  -- color : #{color_param}, mileage: #{mileage_param} .")

    # Car make
    car_make_param = "#{row["MAKE"]}"
    car_make_unknown = CarMake.find_by(make: "Not Specified")
    car_make = CarMake.find_by(make: car_make_param)
    unless car_make.nil? then
      car_make_id = car_make.id
    else
      car_make_id = car_make_unknown.id
      log_file.puts("  -- #{car_make_param} not found.")
    end
	  
    new_car = Car.new(vehicle_type_id: vehicle_type_id, capacity: capacity_param, year: car_year_param, 
      	              car_model_id: car_model_id, transmission_type_id: transmission_type_id, car_make_id: car_make_id,
                      fuel_type_id: fuel_type_id, color: color_param, mileage: mileage_param, user_id: user.id)

    unless new_car.save then
	  	log_file.puts("  Failed : name : #{car_owner_fullname_param}")
	  	log_file.puts("  Reason : Unable to create car. " )
        # Skip creating details
	  	next
	  else
	    log_file.puts("  Record #{i}: #{car_owner_fullname_param} car created.")
	  end

      # Car Owner/Operator
	  operator_name_param = "#{row["OPERATOR NAME"]}"
	  operator_contact_info_param = "#{row["OPERATOR CONTACT INFO"]}"
	  comments_param = "Comment" #+ "#{row["COMMENTS"]}"
	  other_features_param = "Features" #+ "#{row["OTHER FEATURES"]}"
    vehicle_nickname_param = "#{row["VEHICLE NICKNAME"]}"
	  vehicle_relationship_param = "#{row["VEHICLE RELATIONSHIP"]}"
	  
	  driving_rules_param = "#{row["DRIVING RULES"]}"
	  driving_rule_unknown = DrivingRule.find_by(description: "Not Specified")
    driving_rule = DrivingRule.find_by(description: driving_rules_param)
	  unless driving_rule.nil? then
	  	driving_rule_id = driving_rule.id
	  else
	  	driving_rule_id = driving_rule_unknown.id
	    log_file.puts("  -- #{driving_rules_param} not found.")
	  end

	  delivery_options_param = "#{row["DELIVERY OPTIONS"]}"
	  delivery_option_unknown = DeliveryOption.find_by(description: "Not Specified")
    delivery_option = DeliveryOption.find_by(description: delivery_options_param)
	  unless delivery_option.nil? then
	  	delivery_option_id = delivery_option.id
	  else
	  	delivery_option_id = delivery_option_unknown.id
	    log_file.puts("  -- #{delivery_options_param} not found.")
	  end

	  vehicle_location_param = "#{row["VEHICLE LOCATION"]}"
	  driver_location_param = "#{row["DRIVER LOCATION"]}"

	  # Payment Details
 	  payment_frequency_param = "#{row["PAYMENT FREQUENCY"]}"
    payment_frequency = PaymentFrequency.find_by(description: payment_frequency_param)
	  unless payment_frequency.nil? then
	  	payment_frequency_id = payment_frequency.id
	  else
	  	payment_frequency_id = 1
	    log_file.puts("  -- #{payment_frequency_param} not found.")
	  end

	  payment_method_param = "#{row["PAYMENT METHOD"]}"
	  payment_method_unknown = PaymentMethod.find_by(description: "Not Specified")
      payment_method = PaymentMethod.find_by(description: payment_method_param)
	  unless payment_method.nil? then
	  	payment_method_id = payment_method.id
	  else
	  	payment_method_id = payment_method_unknown.id
	    log_file.puts("  -- #{i}: #{payment_method_param} not found.")
	  end

	  cost_city_driving_param = "#{row["COST CITY DRIVING"]}"
	  cost_provincial_driving_param = "#{row["COST PROV DRIVING"]}"
	  cost_inclusion_param = "#{row["COST INCLUSION"]}"
	  payment_spec_param = "#{row["PAYMENT SPEC"]}"
	  additional_rules_param = "#{row["ADDTL RULES"]}"
	  
    listing_detail = ListingDetail.new(operator_name: operator_name_param, operator_ci: operator_contact_info_param, 
      	comments: comments_param, other_features: other_features_param, vehicle_nickname: vehicle_nickname_param, 
      	vehicle_relationship: vehicle_relationship_param, driving_rule_id: driving_rule_id, delivery_option_id: delivery_option_id, 
      	vehicle_location: vehicle_location_param, driver_location: driver_location_param, payment_frequency_id: payment_frequency_id, 
      	payment_method_id: payment_method_id, cost_city_driving: cost_city_driving_param[1..80], cost_prov_driving: cost_provincial_driving_param[1..80], 
      	cost_inclusion: cost_inclusion_param, payment_spec: payment_spec_param, addtl_rule: additional_rules_param, car_id: new_car.id)

    unless listing_detail.save then
	  	log_file.puts("Failed : Record #{i} name : #{car_owner_fullname_param}")
	  	log_file.puts("Reason : Unable to create listing detail. " )
        # Skip creating details
	  	next
	  else
	    log_file.puts("Record #{i}: #{car_owner_fullname_param} listing detail created.")
	  end
	  log_file.puts("")	  

	end

	#teardown
	print "finished(#{Time.now.strftime('%Y%m%d%H%M%S').to_i - start_time.to_i} seconds) \n"
	puts "see logs here:"
	puts "    #{log_dir[0]}"
	spreadsheet.close()
	log_file.close

end
