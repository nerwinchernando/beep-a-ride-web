# syntax => rake migrate_car_models[CarBrandsModels.xlsx]

task :migrate_car_models, [:file] => :environment do |t,xls|

	puts ""
	print "Migrating Car Models data.\n"

	#log files (setup)
	start_time = Time.now.strftime('%Y%m%d%H%M%S')
	log_dir = FileUtils.mkpath Rails.root.join('log','migration', 'car_models')
	log_file = File.new(log_dir[0] + '/' + start_time + '.log', 'w')
	
	#file migration
	file = Dir.pwd + "/lib/tasks/" + xls[:file]
	spreadsheet = Roo::Spreadsheet.open(file)
	spreadsheet.default_sheet = '2016'
	header = spreadsheet.row(1)
	
    brands = []
	(2..spreadsheet.last_row).each do |i|
	  row = Hash[[header, spreadsheet.row(i)].transpose]
	  if row["Brands"].nil? then
	    break
	  end
	  brands << row["Brands"]
	end
	
	for brand in brands do
	  print "\nGetting car models of " + brand + "\n"
    if CarMake.where(make: brand).take.nil? then
	  	CarMake.create(make: brand)
    end
	  
	  car_make_id = CarMake.where(make: brand).take.id

	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    if row[brand].nil? then
	      break
	    end
	    print "."
	    
	    CarModel.create(model: row[brand], car_make_id: car_make_id)
	    log_file.puts("Record #{i}: { brand: row[brand] }")
	  end
	  log_file.puts("")
  end

	#teardown
	print "finished(#{Time.now.strftime('%Y%m%d%H%M%S').to_i - start_time.to_i} seconds) \n"
	puts "see logs here:"
	puts "    #{log_dir[0]}/#{log_file}"
	spreadsheet.close()
	log_file.close

end