# syntax => rake migrate_whitelist[BeepDB_2016.xlsx]

task :migrate_whitelist, [:file] => :environment do |t,xls|

	puts ""
	print "Migrating Whitelist data."

	#log files (setup)
	#Dir[File.join(Rails.root.join('log','migration', 'whitelist', '*.log'))].map{|n| remove_file(n)}
	start_time = Time.now.strftime('%Y%m%d%H%M%S')
	log_dir = FileUtils.mkpath Rails.root.join('log','migration', 'whitelist')
	log_file = File.new(log_dir[0] + '/' + start_time + '.log', 'w')
	
	#file migration
	file = Dir.pwd + "/lib/tasks/" + xls[:file]
	spreadsheet = Roo::Spreadsheet.open(file)
	header = spreadsheet.row(1)
	(2..spreadsheet.last_row).each do |i|
	  row = Hash[[header, spreadsheet.row(i)].transpose]
	  print "."

	  generated_password = Devise.friendly_token.first(8)
	  user = User.new(email: "#{row["EMAIL"]}", password: generated_password)

	  unless user.save
	  	log_file.puts("Failed : Record #{i} name : #{row["NAME"]}")
		log_file.puts("Reason : #{row["EMAIL"]} already exists! " )
		log_file.puts("")
	  else
	  	profile = Profile.create(full_name: "#{row["NAME"]}", 
	  		                     priority: "#{row["#"]}",
	  		                     phone_number: "#{row["Car Owner's Contact Number"]}",
	  		  				     date_time_received: "#{row["DTR"]}", 
	  		  				     user_id: user.id)
	  	log_file.puts("Record #{i}: { email: #{row["EMAIL"]}, password: #{generated_password} }")
		log_file.puts("")
	  end	
	end

	#teardown
	print "finished(#{Time.now.strftime('%Y%m%d%H%M%S').to_i - start_time.to_i} seconds) \n"
	puts "see logs here:"
	puts "    #{log_dir[0]}/#{log_file}"
	spreadsheet.close()
	log_file.close

end