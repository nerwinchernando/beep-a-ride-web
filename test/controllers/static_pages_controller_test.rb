require "test_helper"

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  def test_about
    get about_url
    assert_response :success
  end

  def test_contact
    get contact_url
    assert_response :success
  end

  def test_terms_and_conditions
    get terms_and_conditions_url
    assert_response :success
  end

end
