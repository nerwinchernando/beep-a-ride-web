FactoryGirl.define do
  factory :reservation do
    user nil
    car nil
    start_date ""
    end_date ""
    price 1
    total 1
  end
end
