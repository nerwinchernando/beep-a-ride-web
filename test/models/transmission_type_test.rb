require 'test_helper'

class TransmissionTypeTest < ActiveSupport::TestCase
  def setup
    @transmission_type = TransmissionType.new(description: "Manual")
  end

  test "transmission type should be valid" do
    assert @transmission_type.valid?
  end

  test "description should be present" do
    @transmission_type.description = " "
    assert_not @transmission_type.valid?
  end

  test "description should be unique" do
    @transmission_type.save
    description2 = TransmissionType.new(description: "Manual")
    assert_not description2.valid?
  end

  test "description should not be too long" do
    @transmission_type.description = "a" * 26
    assert_not @transmission_type.valid?
  end

  test "description should not be too short" do
    @transmission_type.description = "aa"
    assert_not @transmission_type.valid?
  end

end