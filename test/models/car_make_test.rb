require 'test_helper'

class CarMakeTest < ActiveSupport::TestCase
  def setup
    @car_make = CarMake.new(make: "Toyota")
  end

  test "car make should be valid" do
    assert @car_make.valid?
  end

  test "car make should be present" do
    @car_make.make = " "
    assert_not @car_make.valid?
  end

  test "description should be unique" do
    @car_make.save
    car_make2 = CarMake.new(make: "Toyota")
    assert_not car_make2.valid?
  end

  test "description should not be too long" do
    @car_make.make = "a" * 26
    assert_not @car_make.valid?
  end

  test "description should not be too short" do
    @car_make.make = "a"
    assert_not @car_make.valid?
  end

end