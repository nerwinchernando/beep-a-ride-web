require 'test_helper'

class PaymentMethodTest < ActiveSupport::TestCase
  def setup
    @payment_method = PaymentMethod.new(description: "Cash", payment_bit: 1)
  end

  test "payment method should be valid" do
    assert @payment_method.valid?
  end

  test "description should be present" do
    @payment_method.description = " "
    assert_not @payment_method.valid?
  end

  test "description should be unique" do
    @payment_method.save
    description2 = PaymentMethod.new(description: "Cash", payment_bit: 1)
    assert_not description2.valid?
  end

  test "description should not be too long" do
    @payment_method.description = "a" * 51
    assert_not @payment_method.valid?
  end

  test "description should not be too short" do
    @payment_method.description = "aa"
    assert_not @payment_method.valid?
  end

end