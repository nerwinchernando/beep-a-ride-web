require 'test_helper'

class PaymentFrequencyTest < ActiveSupport::TestCase
  def setup
    @payment_frequency = PaymentFrequency.new(description: "Hourly", frequency_bit: 1)
  end

  test "payment frequency should be valid" do
    assert @payment_frequency.valid?
  end

  test "description should be present" do
    @payment_frequency.description = " "
    assert_not @payment_frequency.valid?
  end

  test "description should be unique" do
    @payment_frequency.save
    description2 = PaymentFrequency.new(description: "Hourly", frequency_bit: 1)
    assert_not description2.valid?
  end

  test "description should not be too long" do
    @payment_frequency.description = "a" * 51
    assert_not @payment_frequency.valid?
  end

  test "description should not be too short" do
    @payment_frequency.description = "aa"
    assert_not @payment_frequency.valid?
  end

end