require 'test_helper'

class CarTest < ActiveSupport::TestCase
  def setup
    # id                   | 4
    # vehicle_type_id      | (1: Not Specified, 2: Sedan, .. )
    # capacity             | 5
    # year                 | 2015
    # car_model_id         | 1
    # transmission_type_id | (1: Not Specified, 2: Automatic, 3: Manual)
    # fuel_type_id         | (1: Not Specified, 2: Gasoline, 3: Diesel)
    # color                | Cool Silver
    # mileage              | 10000
    # user_id              | 4
    # created_at           | 2016-12-18 07:27:33.838077
    # updated_at           | 2016-12-18 07:27:33.838077
    # active               | t
    @car = Car.new(vehicle_type_id: 1, capacity: 5, year: 2017, car_model_id: 1, transmission_type_id: 3,
                   fuel_type_id: 1, color: "Red", mileage: 2000, user_id: 4, active: "t")
  end

  test "car should be valid" do
    assert @car.valid?
  end

  test "vehicle type should be present" do
    sedan_car = Car.new(capacity: 5, year: 2017, car_model_id: 1, transmission_type_id: 3,
                   fuel_type_id: 1, color: "Red", mileage: 2000, user_id: 4, active: "t")    
    assert_not sedan_car.valid?
  end

  test "capacity should be present" do
    sedan_car = Car.new(vehicle_type_id: 1, year: 2017, car_model_id: 1, transmission_type_id: 3,
                   fuel_type_id: 1, color: "Red", mileage: 2000, user_id: 4, active: "t") 
    assert_not sedan_car.valid?
  end

  #test "car model should be present" do
  #  sedan_car = Car.new(vehicle_type_id: 1, capacity: 5, year: 2017, transmission_type_id: 3,
  #                 fuel_type_id: 1, color: "Red", mileage: 2000, user_id: 4, active: "t")
  #  assert_not sedan_car.valid?
  #end

  #test "car make should be present" do
  #  sedan_car = Car.new(vehicle_type_id: 1, capacity: 5, year: 2017, car_model_id: 1, transmission_type_id: 3,
  #                 fuel_type_id: 1, color: "Red", mileage: 2000, user_id: 4, active: "t")
  #  assert_not sedan_car.valid?
  #end

  #test "description should be unique" do
  #  @car.save
  #  car2 = Car.new(description: "With Driver")
  #  assert_not car2.valid?
  #end

  #test "description should not be too long" do
  #  @car.description = "a" * 26
  #  assert_not @car.valid?
  #end

  #test "description should not be too short" do
  #  @car.description = "aa"
  #  assert_not @car.valid?
  #end

end