require 'test_helper'

class DrivingRuleTest < ActiveSupport::TestCase
  def setup
    @driving_rule = DrivingRule.new(description: "With Driver")
  end

  test "driving rule should be valid" do
    assert @driving_rule.valid?
  end

  test "description should be present" do
    @driving_rule.description = " "
    assert_not @driving_rule.valid?
  end

  test "description should be unique" do
    @driving_rule.save
    driving_rule2 = DrivingRule.new(description: "With Driver")
    assert_not driving_rule2.valid?
  end

  test "description should not be too long" do
    @driving_rule.description = "a" * 31
    assert_not @driving_rule.valid?
  end

  test "description should not be too short" do
    @driving_rule.description = "aa"
    assert_not @driving_rule.valid?
  end

end