require "test_helper"

class ReservationTest < ActiveSupport::TestCase
  def reservation
    @reservation ||= Reservation.new
  end

  def test_valid
    assert reservation.valid?
  end
end
