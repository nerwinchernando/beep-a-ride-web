require 'test_helper'

class DeliveryOptionTest < ActiveSupport::TestCase
  def setup
    @delivery_option = DeliveryOption.new(description: "With Driver")
  end

  test "delivery option should be valid" do
    assert @delivery_option.valid?
  end

  test "description should be present" do
    @delivery_option.description = " "
    assert_not @delivery_option.valid?
  end

  test "description should be unique" do
    @delivery_option.save
    delivery_option2 = DeliveryOption.new(description: "With Driver")
    assert_not delivery_option2.valid?
  end

  test "description should not be too long" do
    @delivery_option.description = "a" * 26
    assert_not @delivery_option.valid?
  end

  test "description should not be too short" do
    @delivery_option.description = "aa"
    assert_not @delivery_option.valid?
  end

end