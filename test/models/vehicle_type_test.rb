require 'test_helper'

class VehichleTypeTest < ActiveSupport::TestCase
  def setup
    @vehicle_type = VehicleType.new(description: "Sedan")
  end

  test "vehicle type should be valid" do
    assert @vehicle_type.valid?
  end

  test "description should be present" do
    @vehicle_type.description = " "
    assert_not @vehicle_type.valid?
  end

  test "description should be unique" do
    @vehicle_type.save
    description2 = VehicleType.new(description: "Sedan")
    assert_not description2.valid?
  end

  test "description should not be too long" do
    @vehicle_type.description = "a" * 41
    assert_not @vehicle_type.valid?
  end

  test "description should not be too short" do
    @vehicle_type.description = "aa"
    assert_not @vehicle_type.valid?
  end

end