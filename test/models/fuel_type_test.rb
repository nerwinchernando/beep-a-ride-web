require 'test_helper'

class FuelTypeTest < ActiveSupport::TestCase
  def setup
    @fuel_type = FuelType.new(description: "Gasoline")
  end

  test "fuel type should be valid" do
    assert @fuel_type.valid?
  end

  test "description should be present" do
    @fuel_type.description = " "
    assert_not @fuel_type.valid?
  end

  test "description should be unique" do
    @fuel_type.save
    description2 = FuelType.new(description: "Gasoline")
    assert_not description2.valid?
  end

  test "description should not be too long" do
    @fuel_type.description = "a" * 26
    assert_not @fuel_type.valid?
  end

  test "description should not be too short" do
    @fuel_type.description = "aa"
    assert_not @fuel_type.valid?
  end

end