$(function () {
  // Maku: Curtain call
  $(".curtain").stop().animate({ opacity: "hide" }, 1250);

  // fullscreen for mobile
  // if ($(window).width() < 580) {
  //   $("#app-body").on("scroll", function() {
  //     if (screenfull.enabled) {
  //       screenfull.request();
  //     }
  //   });
  // };

  // Tooltips
  $('[data-toggle="tooltip"]').tooltip({ position: { my: "left+15 center", at: "right center" }});

  // Date Pickers
  $(".datepicker").datepicker({
    minDate: 0,
    beforeShow: function() {
      if ($(window).width() < 580) {
        $(".curtain").addClass("component-call").stop().animate({"opacity":"show"}, 500);
      }
    },
    onSelect: function() {
      if ($(window).width() < 580) {
        $(".curtain").removeClass("component-call").stop().animate({"opacity":"hide"}, 500);
      }
    },
  });

  // Maku: Header check if page scrolled
  $(window).scroll(function () {
    checkScroll();
  });

  // Display only first_name from full_name if it is not available
  var fn = $("i.dropdown-toggle .user-full-name").text().split(' '); 
  $("i.dropdown-toggle span, .user-name span").text(fn[0]);

  // #Welcome: How To Function
  $('.step-num').on('click', function() {
    var $index = $(this).parent().index() + 1;

    // Remove/Add active class for .step-num button and hide/show how to image
    $('.step-num').removeClass('active');
    $(this).addClass('active');
    $('.how-images').stop().animate({'opacity':'hide'}, 0, function() {
      $('.how-images-' + $index).stop().animate({'opacity':'show'}, 250);
    });
  });
  
});

// Check Scroll for Header
function checkScroll() {
  var $scrollTop = $(window).scrollTop();
  if ($scrollTop > 34) {
    $("#page-header").addClass("logoDown");
  } else {
    $("#page-header").removeClass("logoDown");
  }
}
;
