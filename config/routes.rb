Rails.application.routes.draw do

  root 'welcome#index'  
  
  resources :cars do
    resources :listing_details, only: [:create]
  end

  resources :photos

  resources :cars do
    resources :reservations, only: [:create]
  end

  get '/preload' => 'reservations#preload'
  get '/preview' => 'reservations#preview'

  get '/your_trips' => 'reservations#your_trips'
  get '/your_reservations' => 'reservations#your_reservations'

  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'
  get '/terms_and_conditions',  to: 'static_pages#terms_and_conditions'

  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  scope :users do 
    get '/profile', to: 'profiles#index'
  	get '/profile/:id/edit', to: 'profiles#edit'
    put '/profile/:id', to: 'profiles#update'
    post '/profile', to: 'profiles#attach_file', as: 'user_attachments'
  end

  devise_for :users,
  					 :controllers => {:registrations => 'registrations'}

  scope '/admin' do
  	get '/archives', to: 'archives#index'
  	post '/archives', to: 'archives#import', as: 'file_import'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #match ':controller(/:action(/:id))', :via => [:get, :post, :put]
end
