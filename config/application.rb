require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
ENV['RAILS_ADMIN_THEME'] = 'rollincode'

module BeepARideWeb
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.generators do |g|
        g.stylesheets = false
        g.javascripts = false
        g.helper = false
        g.test_framework = false
        #g.jbuilder = false
    end
    
    config.generators do |g|
      g.test_framework :minitest, spec: false, fixture: false
      # add this if you're using FactoryGirl
      g.fixture_replacement :factory_girl 
    end
  end
end
