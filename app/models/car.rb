class Car < ApplicationRecord
  belongs_to :user
  belongs_to :fuel_type
  belongs_to :car_model
  belongs_to :car_make
  belongs_to :transmission_type
  belongs_to :vehicle_type
  has_one    :listing_detail
  has_many  :photos
  has_many :reservations

  validates :vehicle_type_id,  presence: true
  validates :capacity,  presence: true
  validates :year,  presence: true
  #validates :car_model_id,  presence: true
  validates :transmission_type_id,  presence: true
  validates :fuel_type_id,  presence: true
  validates :color,  presence: true
  validates :mileage,  presence: true
  #validates :car_make_id,  presence: true
  accepts_nested_attributes_for :listing_detail
  #mount_uploaders :car_photos, CarPhotoUploader
  #validate :image_size_validation

  accepts_nested_attributes_for :listing_detail, :reject_if => :all_blank
  
  private
    #def image_size_validation
    #  errors[:car_photos] << "should be less than 500KB" if car_photos.size > 0.5.megabytes
    #end
end
