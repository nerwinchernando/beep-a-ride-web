class VehicleType < ApplicationRecord
  validates_uniqueness_of :description, :case_sensitive => false
  validates :description, presence: true, length: { minimum: 3, maximum: 40 }
end
