class CarModel < ApplicationRecord
  belongs_to :car_make
  validates :model, presence: true
  validates_uniqueness_of :model, :case_sensitive => false
end
