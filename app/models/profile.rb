class Profile < ApplicationRecord
  belongs_to :user, :dependent => :destroy
  mount_uploader :avatar, AvatarUploader
  # attr_accessible :first_name, :last_name, :gender, :phone_number
end
