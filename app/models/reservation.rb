#  rails g model Reservation user:references car:references 
#    start_date:date_time end_date:date_time price:integer total:integer 
class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :car
end
