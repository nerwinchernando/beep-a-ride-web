class CarMake < ApplicationRecord
  has_many :car_models
  validates :make, presence: true, length: { minimum: 2, maximum: 25 }
  validates_uniqueness_of :make, :case_sensitive => false
end
