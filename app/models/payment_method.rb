class PaymentMethod < ApplicationRecord
  validates :description, presence: true, length: { minimum: 3, maximum: 50 }
  validates :payment_bit, presence: true
  validates_uniqueness_of :description
end
