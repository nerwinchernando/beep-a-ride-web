class ListingDetail < ApplicationRecord
  belongs_to :car
  belongs_to :payment_method 
  belongs_to :delivery_option 
  belongs_to :payment_frequency 
  belongs_to :driving_rule

  validates :operator_name,  presence: true 
  validates :operator_ci,  presence: true
  validates :comments,  presence: true 
  validates :other_features,  presence: true 
  validates :vehicle_nickname,  presence: true 
  validates :vehicle_relationship,  presence: true 
  validates :driving_rule_id,  presence: true 
  validates :delivery_option_id,  presence: true 
  validates :vehicle_location,  presence: true 
  validates :driver_location,  presence: true 
  validates :payment_frequency_id,  presence: true 
  validates :payment_method_id,  presence: true
  validates :cost_city_driving,  presence: true 
  validates :cost_prov_driving,  presence: true 
  validates :cost_inclusion,  presence: true 
  validates :payment_spec,  presence: true 
  validates :addtl_rule,  presence: true

  geocoded_by :vehicle_location
  after_validation :geocode, if: :vehicle_location?

  
end
