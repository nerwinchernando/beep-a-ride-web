class DrivingRule < ApplicationRecord
  validates :description, presence: true, length: { minimum: 3, maximum: 30 }
  validates_uniqueness_of :description
end
