class PaymentFrequency < ApplicationRecord
  validates :description, presence: true, length: { minimum: 3, maximum: 50 }
  validates :frequency_bit, presence: true
  validates_uniqueness_of :description
end
