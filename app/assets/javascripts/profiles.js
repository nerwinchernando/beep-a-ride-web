$(function(){

	// Show attachement blocks
	$(".profile-requirements .add-req-btn").on("click", function() {
		$(".add-req-btn, .renter-requirements, .owner-requirements").stop().animate({'opacity':'hide'}, function() {
			$(".req-add, .submit-req, .btn_cancel").stop().animate({'opacity':'show'});
		});
		
	});

	// Hide when cancel
	$(".btn_cancel").on("click", function() {
		$(".req-add, .submit-req, .btn_cancel").stop().animate({'opacity':'hide'}, function() {
			$(".add-req-btn, .renter-requirements, .owner-requirements").stop().animate({'opacity':'show'});
		});

	});

});