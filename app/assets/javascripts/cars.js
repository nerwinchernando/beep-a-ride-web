$(function() {
	
	// New : Next Tab Content func
	$('.to-next-block .btn-next').on('click', function(){

		// hide car models 
		$('.car-model').stop().animate({'opacity':'hide'}, 100);

		$(this).parent().parent().stop().animate({'opacity':'hide'}, 100, function() {

			// change header
			var next = $(this).index() + 1;
			$('.process-block .detail-title').eq(next - 1).addClass("done");
			$('.process-block .detail-title').eq(next).addClass("active");

			// change content
			$(this).next().stop().animate({'opacity':'show'});

			// show submit when last tab is active
			if(next == 2) {
				$('.form-actions.hide').stop().animate({'opacity':'show'});
			}

		});
	});

	// New : Back func
	$('.btn-back').on('click', function() {
		$(this).parent().parent().stop().animate({'opacity':'hide'}, 100, function() {

			// change header
			var now = $(this).index();
			$('.process-block .detail-title').eq(now).removeClass("active");
			$('.process-block .detail-title').eq(now - 1).removeClass("done");

			//console.log($(this).attr('class'));

			// change content
			$(this).prev().stop().animate({'opacity':'show'});
			console.log($(this).attr("class"));

			// show submit when last tab is active
			if(now == 1) {
				// hide car models 
				$('.car-model').stop().animate({'opacity':'show'}, 100);

			}

		});
	});

	// New : Vehicle Detail Tab - change car model
	$('#car_vehicle_type_id').change(function() {
		var carType = $(':selected',this).text().replace(/\s+/g, '-').toLowerCase();;
	  	console.log(carType);
	    $('.car-model-pic').stop().animate({'opacity':'hide'}, 100, function() {
	    	$('.car-model-pic').removeClass();
	    	$('.car-models div').addClass('car-model-pic ' + carType);
	    	$('.car-model-pic').stop().animate({'opacity':'show'});
	    });
	});

});