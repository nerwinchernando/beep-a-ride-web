class ArchivesController < ApplicationController

  def index
  end

	def import
		@data = parse_archived(params[:file])
		# parse_and_save_archived(params[:file])
		render :index #, notice: 'File Import Succes'
	end

#######################
## FLOW
#  - import file => validate file => parse => validate elements => render
#  - options to save or update and save
#
#######################
## TODO ?
# => create file validation
# 			=> has one sheet
# 			=> has xls format
# 			=> parsing header
# => create database validation
# 			=> user exist in db?
# => add database validation status in table
# => create controller actions: update(update and save) and save(save_only)
#
#


	private

	def parse_archived(file)
		spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  header.each_with_index do |element, index|
	    header[index] = "date_filed" if element == "Timestamp"
	    header[index] = "attribute" if element == "What is your relationship to the vehicle?"
	    header[index] = "rules" if element == "What driving rules are you open for with your vehicle?"
	    header[index] = "delivery" if element == "What delivery options do you prefer?"
	    header[index] = "car_name" if element == "Vehicle Nickname"
	    header[index] = "car_location" if element == "Location of Vehicle"
	    header[index] = "driver_location" if element == "Location of Driver/s"
	    header[index] = "vehicle_types" if element == "Type of Vehicle"
	    header[index] = "car_capacity" if element == "Capacity"
	    header[index] = "car_make" if element == "Make"
	    header[index] = "car_year" if element == "Year"
	    header[index] = "car_model" if element == "Model"
	    header[index] = "car_transmission" if element == "Transmission"
	    header[index] = "car_fuel" if element == "Fuel"
	    header[index] = "car_color" if element == "Color"
	    header[index] = "car_mileage" if element == "Mileage (for automobiles)"
	    header[index] = "car_other_feature" if element == "Other features/specifications/uniqueness"
	    header[index] = "payment_freq" if element == "Payment Frequency"
	    header[index] = "payment_method" if element == "Payment Method"
	    header[index] = "price_city" if element == "Cost/Price for City Driving"
	    header[index] = "price_province" if element == "Cost/Price for Provincial Driving"
	    header[index] = "cost_inclusion" if element == "Cost inclusions"
	    header[index] = "payment_condition" if element == "Additional payment specifications"
	    header[index] = "rules_addition" if element == "Additional rules/preferences/notes for the renter"
	    header[index] = "user_full_name" if element == "Car Owner's Full Name"
	    header[index] = "user_email" if element == "Car Owner's Email"
	    header[index] = "user_number" if element == "Car Owner's Contact Number"
	    # header[index] = "logic" if element == "Operator's Name"
	    # header[index] = "logic" if element == "Operator's Contact Information"
	    header[index] = "user_comment" if element == "Additional comment/request/suggestion"
	  end
	  data = Array.new
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose].except!("DT")
	    # row.except!("DT")
	    # row["date_filed"] = row.delete "Timestamp"
	    # row["attribute"] = row.delete "What is your relationship to the vehicle?"
	    # row["rules"] = row.delete "What driving rules are you open for with your vehicle?"
	    # row["delivery"] = row.delete "What delivery options do you prefer?"
	    # row["car_name"] = row.delete "Vehicle Nickname"
	    # row["car_location"] = row.delete "Location of Vehicle"
	    # row["driver_location"] = row.delete "Location of Driver/s"
	    # row["vehicle_types"] = row.delete "Type of Vehicle"
	    # row["car_capacity"] = row.delete "Capacity"
	    # row["car_make"] = row.delete "Make"
	    # row["car_year"] = row.delete "Year"
	    # row["car_model"] = row.delete "Model"
	    # row["car_transmission"] = row.delete "Transmission"
	    # row["car_fuel"] = row.delete "Fuel"
	    # row["car_color"] = row.delete "Color"
	    # row["car_mileage"] = row.delete "Mileage (for automobiles)"
	    # row["car_other_feature"] = row.delete "Other features/specifications/uniqueness"
	    # row["payment_freq"] = row.delete "Payment Frequency"
	    # row["payment_method"] = row.delete "Payment Method"
	    # row["price_city"] = row.delete "Cost/Price for City Driving"
	    # row["price_province"] = row.delete "Cost/Price for Provincial Driving"
	    # row["cost_inclusion"] = row.delete "Cost inclusions"
	    # row["payment_condition"] = row.delete "Additional payment specifications"
	    # row["rules_addition"] = row.delete "Additional rules/preferences/notes for the renter"
	    # row["user_full_name"] = row.delete "Car Owner's Full Name"
	    # row["user_email"] = row.delete "Car Owner's Email"
	    # row["user_number"] = row.delete "Car Owner's Contact Number"
	    # row["logic"] = row.delete "Operator's Name"
	    # row["logic"] = row.delete "Operator's Contact Information"
	    # row["user_comment"] = row.delete "Additional comment/request/suggestion"

	    data << row 
	    # user = User.where(email: row["Car Owner's Email"]) || User.new
	    # user.attributes = row.to_hash
	    # user.save!
	    # user.car
	  end
	  return data
	end

end
