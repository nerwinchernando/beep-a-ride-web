class RegistrationsController < Devise::RegistrationsController

  after_action :create_profile, only: [:create]

  private
	def create_profile
	  Profile.create({
		:first_name => params[:user][:user][:first_name],
		:last_name => params[:user][:user][:last_name],
		:user_id => resource.id,
		:gender => params[:user][:user][:gender],
		:phone_number => ''
	  })
	end

  protected
	def update_resource(resource, params)
	  resource.update_without_password(params)
	end

	# def configure_permitted_parameters
	#   devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :email, :password) }
	# end
end
