class CarsController < ApplicationController
  before_action :set_car, only: [:show, :edit, :update]
  before_action :authenticate_user!, except: [:show]

  def index
    @cars = current_user.cars.joins(:listing_detail)
  end

  def show
    @photos = @car.photos
  end

  def new
    @car = current_user.cars.build
    @listing_detail = @car.build_listing_detail
  end

  def create
    @car = current_user.cars.build(car_params)

    if @car.save

      if params[:images] 
        params[:images].each do |image|
          @car.photos.create(image: image)
        end
      end

      @photos = @car.photos
      redirect_to edit_car_path(@car), notice: "Registered car successfully..."
    else
      render :new
    end
  end

  def edit
    if current_user.id == @car.user.id
      @photos = @car.photos
      unless @car.listing_detail
        @listing_detail = @car.build_listing_detail
      end
    else
      redirect_to root_path, notice: "You don't have permission to edit this."
    end
  end

  def update
    if @car.update(car_params)
      if params[:images] 
        params[:images].each do |image|
          @car.photos.create(image: image)
        end
      end

      redirect_to edit_car_path(@car), notice: "Updated car successfully..."
    else
      render :edit
    end
  end

private
  def set_car
    @car = Car.find(params[:id]) 
  end

  def car_params
    params.require(:car).permit(:vehicle_type_id, :capacity, :year, :car_model_id, :fuel, :color, 
       :mileage, :car_make_id, :fuel_type_id, :transmission_type_id, {car_photos: []}, listing_detail_attributes: [:operator_ci, :operator_name,
        :vehicle_nickname, :vehicle_relationship, :vehicle_location, :driver_location, :driving_rule_id, :delivery_option_id,
        :cost_city_driving, :cost_prov_driving, :payment_frequency_id, :payment_method_id, :cost_inclusion,
        :addtl_rule, :payment_spec, :other_features, :comments, :car_id])
  end 
end
