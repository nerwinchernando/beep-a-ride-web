class ProfilesController < ApplicationController
  before_action :authenticate_user!

  def index
    
  end

  def show
  end

  def edit
  end

  def update
    current_user.profile.update(profile_params)
    respond_to do |format|
      format.html {redirect_to action: 'show'}
      format.js {render inline: "location.reload();" }
    end 
  end

  def attach_file
    logger.debug category = params[:attachment].keys[0]
    name = Dir[Rails.root.join("private/user_attachments/"+"#{current_user.id}/*"+category)].count.next.to_s + ".png"
    directory = FileUtils.mkpath Rails.root.join("private", "user_attachments", "#{current_user.id}", category)
    path = File.join(directory, name)
    File.open(path, "wb") { |f| f.write(params[:attachment][category].read) }
    flash[:notice] = "File uploaded"
    redirect_to "/profiles/index"
  end

  private

  def profile_params
    params[:user].fetch(:profile, {}).permit(:first_name, :last_name, :date_of_birth, :referral_code, :address, :gender, :phone_number)
  end
end
