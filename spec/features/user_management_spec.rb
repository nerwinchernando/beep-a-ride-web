require 'rails_helper'

feature 'User' do
  include SessionHelpers
  context "registration" do
    before(:each) do
      visit '/'
      @user = FactoryGirl.build(:user)
    end

    scenario "user signup and confirm in email to sign in" do
      sign_up_with(@user.email, @user.password, "Jackie", "Chan")
      expect(UserMailer).to(receive(:deliver_signup).with("email@example.com", "Jackie Chan"))
      signin(@user.email, @user.password)
    end

    scenario "user unable to sign in if didn't confirm in email" do
      sign_up_with(@user.email, @user.password, "Jackie", "Chan")
      signin(@user.email, @user.password)
    end
  end

  xcontext "upload requiremets" do
    before(:each) do
			user = FactoryGirl.create(:user)
      signin(user.email, user.password)
      visit '/users/profile'
      click_link "Attachments"
    end

    scenario "upload car owner orcr", js: true do
      click_link "Car Owner"
      find('label', :text => 'ORCR').find('+form label').click
    end
  end
end
