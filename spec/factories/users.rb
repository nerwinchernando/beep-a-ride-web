FactoryGirl.define do
  factory :user do
    email 'customer@domain.com'
    password 'passw0rd'
    after(:create) do |user|
      create(:profile, user: user)
    end
  end
end