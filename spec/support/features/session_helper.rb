module SessionHelpers
  def sign_up_with(email, password, firstname, lastname)
    visit new_user_registration_path
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    fill_in 'user_user_first_name', :with => firstname
    fill_in 'user_user_last_name', :with => lastname
    click_button 'Join'
  end

  def signin(email, password)
    visit new_user_session_path
    fill_in 'user_email', with: email, :match => :prefer_exact
    fill_in 'user_password', with: password, :match => :prefer_exact
    click_on("Log in", :match => :prefer_exact)
  end
end
