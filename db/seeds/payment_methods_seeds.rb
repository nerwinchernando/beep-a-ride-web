# rake db:seed:payment_method_seeds
# Legend:
#   P - Cash
#   C - Credit Card
#   G - GCash
#   B - Bank Transfer
#   Y - Paypal
#   S - Smart Mony
#   S Y B G C P 
# 1 1 1 1 1 1 1
PaymentMethod.delete_all
PaymentMethod.create!(payment_bit:  1, description: 'Cash')
PaymentMethod.create!(payment_bit:  2, description: 'Credit Card')
PaymentMethod.create!(payment_bit:  3, description: 'Cash, Credit Card')
PaymentMethod.create!(payment_bit:  4, description: 'GCash')
PaymentMethod.create!(payment_bit:  8, description: 'Bank Transfer')
PaymentMethod.create!(payment_bit: 10, description: 'Cash, Bank Transfer')
PaymentMethod.create!(payment_bit: 15, description: 'Cash, GCash, Bank Transfer')
PaymentMethod.create!(payment_bit: 31, description: 'Cash, Paypal, GCash, Bank Transfer')
PaymentMethod.create!(payment_bit: 47, description: 'Cash, Smart Money, Bank Transfer')
PaymentMethod.create!(payment_bit: 64, description: 'Not Specified')
