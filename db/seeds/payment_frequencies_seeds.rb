# rake db:seed:payment_frequencies_seeds
#       M W D H 
# 1 1 1 1 1 1 1
PaymentFrequency.delete_all
PaymentFrequency.create!(frequency_bit:  1, description: 'Hourly')
PaymentFrequency.create!(frequency_bit:  2, description: 'Daily')
PaymentFrequency.create!(frequency_bit:  3, description: 'Hourly, Daily')
PaymentFrequency.create!(frequency_bit:  4, description: 'Weekly')
PaymentFrequency.create!(frequency_bit:  7, description: 'Hourly, Daily, Weekly')
PaymentFrequency.create!(frequency_bit:  8, description: 'Monthly')
PaymentFrequency.create!(frequency_bit: 12, description: 'Weekly, Monthly')
PaymentFrequency.create!(frequency_bit: 14, description: 'Daily, Weekly, Monthly')
PaymentFrequency.create!(frequency_bit: 15, description: 'Hourly, Daily, Weekly, Monthly')
PaymentFrequency.create!(frequency_bit: 16, description: 'Not Specified')

