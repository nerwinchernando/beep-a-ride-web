# rake db:seed:fuel_types_seeds

FuelType.delete_all
FuelType.create!(description: 'Not Specified')
FuelType.create!(description: 'Gasoline')
FuelType.create!(description: 'Diesel')
FuelType.create!(description: 'Liquefied Petroleum')
FuelType.create!(description: 'Compressed Natural Gas')
FuelType.create!(description: 'Ethanol')
FuelType.create!(description: 'Bio-diesel')