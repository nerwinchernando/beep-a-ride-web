# rake db:seed:vehicle_types_seeds

VehicleType.create!(description: 'Not Specified')
VehicleType.create!(description: 'Sedan')
VehicleType.create!(description: 'Multi Purpose Vehicle')
VehicleType.create!(description: 'Hatchback')
VehicleType.create!(description: 'AUV')
VehicleType.create!(description: 'SUV')
VehicleType.create!(description: 'Van')
VehicleType.create!(description: 'SHUTTLE')
VehicleType.create!(description: 'All Purpose Vehicle')
VehicleType.create!(description: 'Mini Multi Purpose Vehicle')
