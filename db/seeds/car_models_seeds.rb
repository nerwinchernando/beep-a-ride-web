# rake db:seed:car_models_seeds

# Catch all
car_make_id = CarMake.where(make: 'Not Specified').take.id
CarModel.create!(model: 'Not Specified', car_make_id: car_make_id)
