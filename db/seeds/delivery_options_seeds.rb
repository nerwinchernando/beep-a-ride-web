# rake db:seed:delivery_options_seeds
DeliveryOption.create!(description: 'Not Specified')
DeliveryOption.create!(description: 'Pickup by renter')
DeliveryOption.create!(description: 'Deliver to renter')
DeliveryOption.create!(description: 'Both')
