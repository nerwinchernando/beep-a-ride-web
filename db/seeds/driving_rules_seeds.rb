# rake db:seed:driving_rules_seeds

DrivingRule.delete_all
DrivingRule.create!(description: 'Not Specified')
DrivingRule.create!(description: 'Self-driven (without driver)')
DrivingRule.create!(description: 'Chauferred (with driver)')
DrivingRule.create!(description: 'Both')

