# rake db:seed:transmission_types_seeds

TransmissionType.delete_all
TransmissionType.create!(description: 'Not Specified')
TransmissionType.create!(description: 'Automatic')
TransmissionType.create!(description: 'Manual')
