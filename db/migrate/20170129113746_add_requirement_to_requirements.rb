class AddRequirementToRequirements < ActiveRecord::Migration[5.0]
  def change
    add_column :requirements, :requirement, :string
  end
end
