class CreatePaymentMethods < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_methods do |t|
      t.integer :payment_bit
      t.string :description

      t.timestamps
    end
  end
end
