class AddPriorityToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :priority, :integer
    add_column :profiles, :source, :string    
  end
end
