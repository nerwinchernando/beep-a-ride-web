class AddDataTimeReceivedToProfiles < ActiveRecord::Migration[5.0]
  def change
      add_column :profiles, :date_time_received, :timestamp
  end
end
