class CreatePaymentFrequencies < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_frequencies do |t|
      t.string :description
      t.integer :frequency_bit, limit:1

      t.timestamps
    end
  end
end
