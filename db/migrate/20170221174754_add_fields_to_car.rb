class AddFieldsToCar < ActiveRecord::Migration[5.0]
  def change
    add_column :listing_details, :latitude, :float
    add_column :listing_details, :longitude, :float
  end
end
