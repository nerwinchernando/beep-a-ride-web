class CreateDeliveryOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :delivery_options do |t|
      t.string :description

      t.timestamps
    end
  end
end
