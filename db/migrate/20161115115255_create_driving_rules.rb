class CreateDrivingRules < ActiveRecord::Migration[5.0]
  def change
    create_table :driving_rules do |t|
      t.string :description
      t.timestamps
    end
  end
end
