#rails g migration add_avatars_to_users avatars:json

class AddCarPhotosToCars < ActiveRecord::Migration[5.0]
  def change
    add_column :cars, :car_photos, :json
  end
end
