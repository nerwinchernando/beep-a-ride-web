class CreateCars < ActiveRecord::Migration[5.0]
  def change
    create_table :cars do |t|
      t.integer :vehicle_type_id
      t.integer :capacity
      t.integer :year
      t.integer :car_model_id
      t.integer :transmission_type_id
      t.integer :fuel_type_id
      t.string  :color
      t.integer :mileage
      t.integer :user_id

      t.timestamps
    end
  end
end
