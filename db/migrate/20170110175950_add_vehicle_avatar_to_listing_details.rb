class AddVehicleAvatarToListingDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :listing_details, :vehicle_avatar, :string
  end
end
