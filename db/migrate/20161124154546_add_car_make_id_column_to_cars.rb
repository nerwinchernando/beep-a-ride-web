class AddCarMakeIdColumnToCars < ActiveRecord::Migration[5.0]
  def change
    add_column :cars, :car_make_id, :integer
  end
end
