class AddActiveColumnToCars < ActiveRecord::Migration[5.0]
  def change
    add_column :cars, :active, :boolean, default: false
  end
end
