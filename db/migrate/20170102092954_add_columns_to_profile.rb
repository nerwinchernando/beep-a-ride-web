class AddColumnsToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :date_of_birth, :date
    add_column :profiles, :address, :string
    add_column :profiles, :referral_code, :string
  end
end
