class CreateListingDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :listing_details do |t|
      t.string  :operator_name
      t.string  :operator_ci
      t.text    :comments
      t.text    :other_features
      t.string  :vehicle_nickname
      t.string  :vehicle_relationship
      t.integer :driving_rule_id
      t.integer :delivery_option_id
      t.string  :vehicle_location
      t.string  :driver_location
      t.integer :payment_frequency_id
      t.integer :payment_method_id
      t.string  :cost_city_driving
      t.string  :cost_prov_driving
      t.string  :cost_inclusion
      t.text    :payment_spec
      t.text    :addtl_rule
      t.integer :car_id

      t.timestamps
    end
  end
end
